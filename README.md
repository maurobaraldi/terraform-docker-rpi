# terraform-docker-rpi

Terraform for provisioning Docker Server in Raspberry Pi

Terraform Docker provider docs

https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs/resources/container

## Install Docker - Ubuntu 20.04 (Raspberry Pi)

```
apt update
apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt update
apt-cache policy docker.io
apt install docker.io
systemctl status docker
adduser deploy
usermod -aG docker deploy
```

## Configure Docker for remote connection (Local)

`export DOCKER_HOST=ssh://deploy@raspberry_pi_address`



## Install Terraform (Local)

```
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
terraform -install-autocomplete
```

## Plan + Apply

Initiate the terraform state

`terraform init`

Apply the changes

`terraform apply`
